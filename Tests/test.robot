*** Settings ***
Library    SeleniumLibrary

*** Variables ***
${BROWSER}    Chrome
${URL}    https://www.google.com
${OPTIONS}    headless,disable-gpu,no-sandbox,disable-dev-shm-usage

*** Test Cases ***
Naviguer vers Google et fermer le navigateur
    [Documentation]    Ce test ouvre le navigateur, navigue vers Google, puis ferme le navigateur.
    Open Browser    ${URL}    headlesschrome
    Maximize Browser Window
    Title Should Be    Google
    Sleep    7s
    Close Browser
